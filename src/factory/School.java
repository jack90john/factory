package factory;

import factory.player.Player;

/**
 * Description: 培养球员的工厂
 * Designer: jack
 * Date: 2018/5/16
 * Version: 1.0.0
 */
class School {

    static <T extends Player> Player createPlayer(Class<T> position) {
        Player player = null;
        try {
            player = position.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return player;
    }

}
