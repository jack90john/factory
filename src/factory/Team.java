package factory;

import factory.player.Defender;
import factory.player.Goalkeeper;
import factory.player.Player;
import factory.player.Striker;

/**
 * Description: 球队让学校培养球员
 * Designer: jack
 * Date: 2018/5/16
 * Version: 1.0.0
 */
public class Team {

    public static void main(String[] args) {
        Player player1 = School.createPlayer(Goalkeeper.class);
        System.out.println("1号球员信息：{ 位置:" +
                player1.getPosition() + "; 特长: " + player1.getSkill() +
                "; 综合评分: " + player1.getScore() + "}");

        Player player2 = School.createPlayer(Defender.class);
        System.out.println("2号球员信息：{ 位置:" +
                player2.getPosition() + "; 特长: " + player2.getSkill() +
                "; 综合评分: " + player2.getScore() + "}");

        Player player3 = School.createPlayer(Striker.class);
        System.out.println("3号球员信息：{ 位置:" +
                player3.getPosition() + "; 特长: " + player3.getSkill() +
                "; 综合评分: " + player3.getScore() + "}");
    }
}
