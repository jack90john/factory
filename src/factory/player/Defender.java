package factory.player;

import java.util.Random;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/21
 * Version: 1.0.0
 */
public class Defender implements Player {

    @Override
    public String getPosition() {
        return "后卫";
    }

    @Override
    public String getSkill() {
        return "拦截意识出众，出球精准";
    }

    @Override
    public Integer getScore() {
        Random random = new Random();
        return 40 + random.nextInt(60);
    }

}
