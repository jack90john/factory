package factory.player;

import java.util.Random;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/21
 * Version: 1.0.0
 */

public class Striker implements Player {

    @Override
    public String getPosition() {
        return "前锋";
    }

    @Override
    public String getSkill() {
        return "盘带细腻，动作敏捷，加速快";
    }

    @Override
    public Integer getScore() {
        Random random = new Random();
        return 40 + random.nextInt(60);
    }
}
