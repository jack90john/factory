package factory.player;

import java.util.Random;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/16
 * Version: 1.0.0
 */
public class Goalkeeper implements Player {

    @Override
    public String getPosition() {
        return "门将";
    }

    @Override
    public String getSkill() {
        return "反应敏捷，脚下技术出众";
    }

    @Override
    public Integer getScore() {
        Random random = new Random();
        return 40 + random.nextInt(60);
    }

}
