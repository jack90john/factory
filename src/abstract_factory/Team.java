package abstract_factory;

import abstract_factory.player.Player;
import abstract_factory.school.Ajax;
import abstract_factory.school.LaMasia;
import abstract_factory.school.School;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class Team {
    public static void main(String[] args) {
        School school1 = new Ajax();
        Player goalkeeper1 = school1.createGoalkeeper();
        System.out.println("阿贾克斯青训营培养的门将信息：{ 位置:" +
                goalkeeper1.getPosition() + "; 特长: " + goalkeeper1.getSkill() +
                "; 综合评分: " + goalkeeper1.getScore() + "}");

        School school2 = new LaMasia();
        Player goalkeeper2 = school2.createGoalkeeper();
        System.out.println("拉玛西亚青训营培养的门将信息：{ 位置:" +
                goalkeeper2.getPosition() + "; 特长: " + goalkeeper2.getSkill() +
                "; 综合评分: " + goalkeeper2.getScore() + "}");
    }
}
