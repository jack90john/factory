package abstract_factory.school;


import abstract_factory.player.Player;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public abstract class AbstractSchool implements School {

    Player createPlayer(Class<? extends Player> clazz) {
        Player human = null;
        try {
            human = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return human;
    }

}