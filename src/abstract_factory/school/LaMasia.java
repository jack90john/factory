package abstract_factory.school;

import abstract_factory.player.Player;
import abstract_factory.player.defender.LaMasiaDefender;
import abstract_factory.player.goalkeeper.LaMasiaGoalkeeper;
import abstract_factory.player.striker.LaMasiaStriker;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class LaMasia extends AbstractSchool {

    @Override
    public Player createGoalkeeper() {
        return createPlayer(LaMasiaGoalkeeper.class);
    }

    @Override
    public Player createDefender() {
        return createPlayer(LaMasiaDefender.class);
    }

    @Override
    public Player createStriker() {
        return createPlayer(LaMasiaStriker.class);
    }

}
