package abstract_factory.school;

import abstract_factory.player.Player;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public interface School {

    Player createGoalkeeper();
    Player createDefender();
    Player createStriker();

}
