package abstract_factory.school;

import abstract_factory.player.Player;
import abstract_factory.player.defender.AjaxDefender;
import abstract_factory.player.goalkeeper.AjaxGoalkeeper;
import abstract_factory.player.striker.AjaxStriker;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class Ajax extends AbstractSchool {
    @Override
    public Player createGoalkeeper() {
        return createPlayer(AjaxGoalkeeper.class);
    }

    @Override
    public Player createDefender() {
        return createPlayer(AjaxDefender.class);
    }

    @Override
    public Player createStriker() {
        return createPlayer(AjaxStriker.class);
    }
}
