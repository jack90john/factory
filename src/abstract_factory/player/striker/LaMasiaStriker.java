package abstract_factory.player.striker;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class LaMasiaStriker extends AbstractStriker {
    @Override
    public String getSkill() {
        return "盘带细腻，动作敏捷，加速快";
    }
}
