package abstract_factory.player.striker;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class AjaxStriker extends AbstractStriker {
    @Override
    public String getSkill() {
        return "身体强壮，力量大，爆发力强";
    }
}
