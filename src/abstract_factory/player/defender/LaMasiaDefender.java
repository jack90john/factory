package abstract_factory.player.defender;

/**
 * Description: 拉玛西亚青训营中的后卫，存放其独特的特性
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class LaMasiaDefender extends AbstractDefender {
    @Override
    public String getSkill() {
        return "拦截意识出众，出球精准";
    }
}
