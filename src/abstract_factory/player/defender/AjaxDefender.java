package abstract_factory.player.defender;

/**
 * Description: 阿贾克斯青训营中的后卫，存放其独特的特性
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class AjaxDefender extends AbstractDefender {
    @Override
    public String getSkill() {
        return "身强体壮，拼抢凶狠";
    }
}
