package abstract_factory.player.defender;

import abstract_factory.player.Player;

import java.util.Random;

/**
 * Description: 后卫抽象类，实现后卫共有的方法
 * Designer: jack
 * Date: 2018/5/16
 * Version: 1.0.0
 */
public abstract class AbstractDefender implements Player {
    @Override
    public String getPosition() {
        return "后卫";
    }

    @Override
    public Integer getScore() {
        Random random = new Random();
        return 40 + random.nextInt(60);
    }
}
