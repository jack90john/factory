package abstract_factory.player.goalkeeper;

import abstract_factory.player.Player;

import java.util.Random;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/16
 * Version: 1.0.0
 */
public abstract class AbstractGoalkeeper implements Player {
    @Override
    public String getPosition() {
        return "门将";
    }

    @Override
    public Integer getScore() {
        Random random = new Random();
        return 40 + random.nextInt(60);
    }
}
