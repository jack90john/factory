package abstract_factory.player.goalkeeper;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class AjaxGoalkeeper extends AbstractGoalkeeper {
    @Override
    public String getSkill() {
        return "擅扑点球";
    }
}
