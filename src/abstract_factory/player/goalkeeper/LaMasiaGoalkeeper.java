package abstract_factory.player.goalkeeper;

/**
 * Description:
 * Designer: jack
 * Date: 2018/5/18
 * Version: 1.0.0
 */
public class LaMasiaGoalkeeper extends AbstractGoalkeeper {
    @Override
    public String getSkill() {
        return "反应敏捷，脚下技术出众";
    }
}
