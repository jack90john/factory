package abstract_factory.player;

/**
 * Description: 球员接口
 * Designer: jack
 * Date: 2018/5/16
 * Version: 1.0.0
 */
public interface Player {
    String getPosition();  //球员位置
    String getSkill();     //球员特长
    Integer getScore();    //球员能力评分
}